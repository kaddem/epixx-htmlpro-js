# README #

Заготовка для занятий по javaScript на продвинутом курсе верстки Epic Skills

## Установка и запуск ##
Установка: `npm i`.

Запуск: `npm start`.

Остановка: <kbd>Ctrl + C</kbd>

## Благодарность ##
За основу взять [репозиторий](https://github.com/epixx/start-kit), подготовленный [Николаем Громовым](http://nicothin.github.io/idiomatic-pre-CSS/) для работы студентов на продвинутом курсе по верстке в [Epic Skills](https://epixx.ru/course_programs/html-pro) 
