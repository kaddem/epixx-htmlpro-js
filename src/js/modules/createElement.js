

function createElement(member, template) {
  const nodeElement = document.createElement(template.tag);
  nodeElement.classList.add(template.tagClass);

  if (template.href) {
    nodeElement.setAttribute('href', template.href);
  }

  nodeElement.innerHTML = template.setContent(member);

  return nodeElement;
}

export default createElement;