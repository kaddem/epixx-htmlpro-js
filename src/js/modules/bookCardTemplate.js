

const bookCardTemplate = {
  wrap: '.j-member-cards',
  tag: 'div',
  tagClass: 'member-card',
  setContent: function(data) {
    return `<h2 class="member__name">${data.name}</h2>
      <p class="member__age">Возраст: ${data.type}</p>
      <img src="/img/books/${data.uri}.png">
      <a href="${data.uri}" class="member__link">${data.uri}</a>`;
  }
}

export default bookCardTemplate;