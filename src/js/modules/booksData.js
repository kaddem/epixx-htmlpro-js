const booksData = {
  members: [
    {
      name: 'Иван',
      age: '22',
      socialLink: 'vk.com',
      socialName: 'вконтакте'
    },{
      name: 'Арина',
      age: '24',
      socialLink: 'facebook.com',
      socialName: 'facebook'
    },{
      name: 'Кирилл',
      age: '24',
      socialLink: 'facebook.com',
      socialName: 'facebook'
    }
  ]
};

export default booksData;